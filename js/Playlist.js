Muziq.prototype.Playlist = {
    
    lists: {},
    quick: [],
    
    init: function(){
        var self = this;
        this.Restore();
        $(document).on('click','div.ui-add',function(e){
            e.preventDefault();
            e.stopPropagation();
            $(this).toggleClass('clicked');
            $('.list-open').addClass('active').delay(1000).queue(function(next){
                $(this).removeClass("active"); next();
            });
            self.quick.push({"artist":$(this).parent().data('artist'),"title":$(this).parent().data('title')});
            self.Store(true);        
            self.RenderQuick();
        });
    },
    
    
    Add: function(artist, title) {
        this.quick.push({"artist":artist,"title":title});
        this.Store(true);        
        this.RenderQuick();
    },
    
    Remove: function(el) {
        $(el).parent().remove();
        this.quick.splice($(el).parent().index(), 1);
        this.Store(true);
    },
    
    RenderAll: function() {
        $('#lists .list').html('<li><a class="ui-btn" data-artist="0">Select Playlist to Play</li>');
        $.each(this.lists, function(k,v){
            var num = "("+v.tracks.length+" tracks)";
            $('#lists .list').append(app.Playlist.RenderTrack(num, v.title));        
        });
           
        $('#lists .list').listview().listview('refresh');
        $('#lists .list a').click(function(){
            var id = $(this).data('title');
            app.Playlist.Title(id);
            app.Playlist.RenderList(app.Playlist.lists[id].tracks);
            $.mobile.changePage('#player');
        });
    },
      
    RenderList: function(tracks) {
        var result = "";
        $.each(tracks, function(k,v){
            var title = app.LastFm.prettify(v.title)+' <small>'+v.artist+'</small>';
            result += "<li class='track' data-artist='"+ v.artist +"' data-title='"+ v.title +"'>"
                    + "<a data-artist='"+ v.artist +"' data-title='"+ v.title +"'>"
					+ title +"</a></li>";   
        });
        $('#player ul.list').html(result).listview().listview('refresh');
    },    
    
    RenderTrack: function(artist, title){
        return '<li data-icon="false"><a data-artist="'+artist+'" data-title="'+title+'" href="#">'+title+' <small>'+artist+'</small></a></li>';
    }, 
        
    RenderQuick: function() {
        $('#playlist .tracks').empty();
        $.each(this.quick, function(k,v){
            $('#playlist .tracks').append(app.Playlist.RenderTrack(v.artist, v.title));        
        });
        $('#playlist .tracks').listview().listview('refresh');
        $('#playlist .tracks a').on('swipeleft', function(e){
            app.Playlist.Remove($(this));
            e.preventDefault();
            return false;
        });
    },

    
    Play: function(){
        this.Title("Quick Queue");
        this.RenderList(this.quick);
        $.mobile.changePage('#player');
    },
    
    Title: function(name) {
        $('#player #tracks-for').text(name);
    },
    
    Save: function() {
        $('#playlist .lists').html('<li><a class="ui-btn" data-artist="0"><em>Save queue to ..</em></li>');
        //$('#playlist .lists').append(app.Playlist.RenderTrack("+", "New Playlist"));     
        $.each(this.lists, function(k,v){
            $('#playlist .lists').append(app.Playlist.RenderTrack(v.tracks.length, v.title));        
        });
           
        $('#playlist .lists').listview().listview('refresh');
        $('#playlist .lists a').click(function(){
           if ($(this).data('artist') == "0") {
                name = prompt("Playlist Name", "New Playlist");
            } else {
                name = $(this).data('title');
            }
            app.Playlist.Update(name);  
            $('#playlist .lists').html('<li data-icon="false"><center>Saved to '+name+'</center></li>');
            setTimeout(function(){
                $('#playlist .lists li').fadeOut(function(){ $(this).remove(); })
            }, 2000);
        });
    },
    
    
    Update: function(id) {
        if (defined(this.lists[id])) {
            $.extend(true, this.lists[id].tracks, this.quick); 
            this.RenderAll();
        }  else {
            this.lists[id] = {
                title: id,
                tracks: this.quick
            };
        }
        this.Store();
    },
    
    Store: function(quick){
        if (quick === true) {
            localStorage.setItem('muziq.quick', JSON.stringify(this.quick));    
        } else {
            localStorage.setItem('muziq.playlist', JSON.stringify(this.lists));    
        }
    },
    
    
    Restore: function(){
        if (!empty(localStorage.getItem('muziq.playlist'))) {
            var _lists = JSON.parse(localStorage.getItem('muziq.playlist'));
            if (defined(_lists)) this.lists = _lists;
        }
        if (!empty(localStorage.getItem('muziq.quick'))) {
            var _quick = JSON.parse(localStorage.getItem('muziq.quick'));
            if (defined(_quick) && _quick.length > 0) this.quick = _quick;
            this.RenderQuick();
        }
    },
    
    onSwipe: function(el) {
        el.on('', function(e){

            e.preventDefault();
            return false;
        });
        
        
    }
    
    
    
    
}