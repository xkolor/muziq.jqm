function Muziq() {

	/* * 
	 * public methods
	 * @method init(module): runs modules init
	 * @method 
	 * */
	this.init = function() {
        app.VK.init();
        app.LastFm.init();
        app.Player.init();
        app.Playlist.init();
	};


}
