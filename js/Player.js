Muziq.prototype.Player = {
	inited: false,
	elem: null,
	duration: 0,
	loading: 0,
	progress: 0,
	loaded: false,

	next: function() {
		if (app.VK.current.dur+1 < app.VK.duration.length) {
			app.VK.current.dur++;
		} else {
			app.VK.current.dur = 0;
		}
		this.play();
	},

	play: function() {
		app.Player.duration = 0;
		app.Player.loading = 0;
		app.Player.progress = 0;
		app.Player.loaded = false;
		$('#player .progress .loaded, #player .progress .seek').css({width:'0%'});

		var source = app.VK.sources[app.VK.duration[app.VK.current.dur]][app.VK.current.src];
		var title = source.title;
		var duration = source.dur;
		var url = source.url;
		var time = mkTime(duration);

		$('#player .now-play .title').text(title);
		$('#player .now-play .time').text(time);
		//$('#player-source').attr('href',url).click();
		$('#player .controls .play-btn').removeClass('play').addClass('pause');

		this.elem.setAttribute('src', url);
		this.elem.load();
		this.elem.play();
	},

	resume: function() {
		this.elem.play();
	},

	pause: function() {
		this.elem.pause();
	},

	init: function() {
		this.inited = true;
		this.elem = new Audio("");
		this.elem.id = "audio";
		this.elem.src = "http://www.w3schools.com/html/horse.mp3";
		this.elem.loop = "";
		this.elem.volume = 1;
		this.elem.load();
		var w = $('#player').width() - 100;
		$('#player .controls .progress').css({width:w+'px'});
		$('#player .controls').slideDown();
		//this.elem.play();
		
		$('#player .play-btn').click(function(){
			if ($(this).hasClass('play')) {
				$(this).removeClass('play').addClass('pause');
				app.Player.resume();
			} else {
				$(this).removeClass('pause').addClass('play');
				app.Player.pause();
			}
		});
		$('#player .source').click(function(){
			//$(this).css('-webkit-transform','rotate(0deg)'); 
			//$(this).css('transform','rotate(0deg)');
			$(this).css('border-spacing',0);
			$(this).animate({borderSpacing: -360 },{
				step: function(now,fx) {
                $(this).css('-webkit-transform','rotate('+now+'deg)'); 
                $(this).css('transform','rotate('+now+'deg)');
				}, duration:'slow'
			},'linear');
			app.Player.next();
		});
		$('#player .progress .loaded').click(function(e){
			var total = $(e.currentTarget).parent().width();
			var current = e.offsetX;
			var seekTo = parseInt((app.Player.duration/total)*current);
			app.Player.elem.currentTime = seekTo;

			console.log(seekTo);
		});

		// bind audio events
		this.elem.onprogress = function(e){
			if (app.Player.loaded) return;
			if (app.Player.duration !== 0) {
				if (this.buffered.end(this.buffered.length-1) > app.Player.loading) {
					app.Player.loading = this.buffered.end(this.buffered.length-1);
					if ((app.Player.duration - app.Player.loading) < 1) {
						app.Player.loaded = true;
					}
					if ((app.Player.loading/app.Player.duration - app.Player.progress) > 0.01) {
						app.Player.progress = (app.Player.loading/app.Player.duration).toFixed(2);
						$('#player .progress .loaded').css({width:app.Player.progress*100+'%'});
					}
				}
			} 
			
		};
		this.elem.ontimeupdate = function(e) {
			$('#player .progress .seek').css({width: (this.currentTime / app.Player.duration)*100+'%'});
		};
		this.elem.onended = function(e){
			$('#player li.playing').next().click();
		};
		this.elem.ondurationchange = function(e){
			if (this.duration !== "NaN") {
				app.Player.duration = this.duration;	
			}
		};
	}
};