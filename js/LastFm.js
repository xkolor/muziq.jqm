Muziq.prototype.LastFm =  {
		api: '//ws.audioscrobbler.com/2.0/?api_key=7f0ae344d4754c175067118a5975ab15&format=json&',
		query: null,
		artist: null,
		mbid: null,
		title: null,
		found : [],
		image: null,
		showing: false,
		tracksFor: null,

		init: function() {
            $('input#search').keyup(function(event){
    			if (event.keyCode == '13') {
					if (app.VK.auth === false) {
						VK.Auth.login(null, VK.access.AUDIO);    
					}
					var q = $('input#search').val();
					if (!empty(q)) {
                        switch(engine) {
                            case 'LF':
                                app.LastFm.getArtists(q);
                                break;
                            case 'DC':
                                app.Discogs.findArtist(q);
                                break;
                            case 'VK':
                                app.Vkontakte.getTracks(q);
                                break;
                        }
						
					}
				}
			});
		},
    
        renderArtists: function(e, key, limit, where){
            var i = 0;
			var result = '';
            var artists = key.split('.').reduce(function index(obj,i) {return obj[i]}, e);
            if (!defined(where)) where = '#artist-search';
			$(artists).each(function(){
				if (++i > limit) return false;
				if (defined(this.image) && !empty(this.image[3]['#text'])) {
					var img = this.image[3]['#text'];
				} else {
					var img = img2 = 'http://i.imgur.com/1jdKzpw.png';
				}
				var name = this.name;
				var url = this.url;
				result += '<div class="artist" data-mbid="'+this.mbid+'" data-artist="'+name+
					'" data-img="'+img+'" style="background-image: url('+img+')">\
					<div class="info ellips">'+name+'</div></div>';
			});
			$(where).html(result);
			$.mobile.silentScroll(100);
        },
        
        
        
        getCountryTop: function(q){
            $.mobile.loading('show');
            if (!defined(q)) q = "Russian Federation";
            var page = Math.floor(Math.random()*20);
        	$.getJSON(this.api+'method=geo.getTopArtists&country='+ q.enc() +'&page='+page+'&callback=?', function(res){
        	    app.LastFm.renderArtists(res, "topartists.artist", 24);
                app.LastFm.initArtists();
            }); 	
        },
        
		getArtists: function(q){
			$('#home .start').slideUp();
			$.mobile.loading('show');
			//$.getJSON(this.api+'method=artist.search&artist='+ q.enc() +'&callback=?', app.LastFm.onGetArtists);	
			$.getJSON(this.api+'method=artist.search&artist='+ q.enc() +'&callback=?', function(res){
    		    app.LastFm.renderArtists(res, "results.artistmatches.artist", 15);
                app.LastFm.initArtists();
			}); 
		},
        
        getSimilar: function(mbid, artist) {
			$.mobile.loading('show');
			var q = defined(mbid)? 'mbid='+ mbid : 'artist='+ artist.enc();
            $.getJSON(this.api+'method=artist.getsimilar&'+q+'&callback=?', function(res){
               //app.LastFm.onGetSimilar(res);
               app.LastFm.renderArtists(res, "similarartists.artist", 18, '#similar .list');
               app.LastFm.initSimilar();
            });
		},

        initArtists: function() {
			var s = this;
			$('#artist-search .artist').click(function(){
				s.image = $(this).css('background-image');
				s.artist = $(this).attr('data-artist');
				s.mbid = $(this).attr('data-mbid');
				$('#albums ul.list').html("<li id='top-tracks'><a>Top Tracks for "+s.artist+"</a></li>");
				$('#albums #top-tracks').click(function(){
					app.LastFm.getTracks(app.LastFm.mbid, app.LastFm.artist);
				});
                if (app.VK.auth === false) {
    				VK.Auth.login(null, VK.access.AUDIO);    
				}
				if (defined(s.mbid) && !empty(s.mbid)) {		
					app.LastFm.getSimilar(s.mbid);    
				} else {
					app.LastFm.getSimilar(null, s.artist);
				}
				app.Discogs.findArtist(s.artist);
			});
			$.mobile.loading('hide');
			$('#artist-search').show();
		},
	
        initSimilar: function() {
			var s = this;
			$('#similar .artist').click(function(){
				s.artist = $(this).attr('data-artist');
				s.mbid = $(this).attr('data-mbid');
				$('#albums ul.list').html("<li id='top-tracks'><a>Top Tracks for "+s.artist+"</a></li>");
				$('#albums #top-tracks').click(function(){
					app.LastFm.getTracks(app.LastFm.mbid, app.LastFm.artist);
				});
				app.Discogs.findArtist(s.artist);
			});
			s.showing = true;
			$.mobile.changePage('#similar');				
			$.mobile.loading('show');
			setTimeout(function(){
				s.showing = false;
				if (app.Discogs.loaded === true) {
					$.mobile.changePage('#albums');	
				}
			}, 2000);
		},

		getTracks: function(mbid, artist) {  
			app.LastFm.tracksFor = artist;
			$.mobile.loading('show');
			if (defined(mbid)) {
				$.getJSON(this.api+'method=artist.gettoptracks&mbid='+ mbid +'&limit=50&callback=?', app.LastFm.onGetTracks);    
			} else {
				$.getJSON(this.api+'method=artist.gettoptracks&artist='+ artist.enc() +'&autocorrect=1&limit=50&callback=?', app.LastFm.onGetTracks);
			}
		},
			 
		onGetTracks: function(e) {
			var s = this;
			s.found = [];
			var result = ''; //'<div class="head">'+ e.toptracks['@attr'].artist +' Tracks</div>';
			$(e.toptracks.track).each(function(){
				var title = app.LastFm.prettify(this.name);
				if (!in_array(title, s.found)) {
                    var id = Math.floor(Math.random()*10000);
					result += '<li id="li'+id+'" class="track" data-artist="'+ this.artist.name +'" data-title="'+ title +'"'
					        + ' draggable="true" ondragenter="enterDrop(event)" ondragleave="exitDrop(event)" ondragover="allowDrop(event)" ondragstart="drag(event)" ondrop="drop(event)">'
                            
                            + '<a data-artist="'+ this.artist.name +'" data-title="'+ title +'" class="ui-btn ui-btn-icon-left ui-icon-bars">'
                            + '<div class="ui-add"></div>'
				            + '<span style="line-height:25px">'+ cap(title) +'</span>'+"<span class='ui-li-count'>"+ this.playcount +"</span></a></li>";       
					s.found.push(title);
				}
			});
			if (empty(s.found)) {
				$.get('lf.php?q='+app.LastFm.artist.enc(), app.LastFm.onGetCharts);
			} else {
				$('#player ul.list').html(result).listview().listview('refresh');
				$('#player #tracks-for').text(app.LastFm.artist +" Top");
                onSwipe($('#player ul.list .track a'));
				$.mobile.changePage('#player');
				$.mobile.loading('hide');
			}
			
		},

		onGetCharts: function(html) {
			app.LastFm.found = [];
			var result = '';
			var div = $(html).find('div.module-body.chart.current');
			div.find('table tr').each(function(k,v){
				var title = app.LastFm.prettify($(v).find('td.subjectCell a').text());
				if (!in_array(title, app.LastFm.found)) {
					result += "<li class='track' data-artist='"+ app.LastFm.tracksFor +"' data-title='"+ title +"'>\
						<a data-artist='"+ app.LastFm.tracksFor +"' data-title='"+ title +"'>"
						+ cap(title) +"</a></li>";  
					app.LastFm.found.push(title);
				} 
			});
			if (app.LastFm.found.length > 0) {
				$('#player ul.list').html(result).listview().listview('refresh');	
				$('#player #tracks-for').text(app.LastFm.tracksFor +" Top");
                onSwipe($('#player ul.list .track a'));
				$.mobile.changePage('#player');
				$.mobile.loading('hide');
			}
		},

		prettify: function(str) {
			str = trimBrackets(str);
			str = str.replace(/( remix| rmx| edit).*/gi,''); // remove (this), 1 word before and everything after
			str = str.replace(/( feat| ft\.| vocals by| vip).*/gi,''); // remove (this) and everything after
			str = str.replace(/(full version|remix|remi| mix|rmx| edit)/gi,''); //remove (this)
			str = str.replace(/(mp3|wav|flac|ogg)/gi,'');
			str = str.replace(/^(A1 |B1 |C1 |D1 |E1 |F1 |G1 |A2 |B2 |C2 |D2 |E2 |F2 )/gi,'');
			return cleanName(str);
		}	
	};