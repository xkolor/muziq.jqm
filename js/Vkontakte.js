Muziq.prototype.VK = {
		auth: false,
		token: null,
		sources: [],
		duration: [],
		api: "http://api.vkontakte.ru/api.php?",
		artist: null,
		title: null,
		durs: [],
		current: {src:0, dur:0},
		audio: null,

		init: function(){
			var s = this;
			VK.init({apiId:1902594, nameTransportPath: '/xd_receiver.html', status: true});
			VK.Observer.subscribe('auth.login', function(response) {
				console.log("VK response", response);
				s.auth = true;
			});    
			VK.Api.call('audio.search', {q: 'spor', sort: 0, count: 10, offset: 0, v: 3, test_mode: 1}, function(r){
				if (defined(r.error)) {
					console.log("VK error occured", r.error);
					s.auth = false;
    				VK.Auth.login(null, VK.access.AUDIO);    
				}
				$('.ui-input-search').fadeIn("slow");
			});

			setTimeout(function(){
				$('.ui-input-search').fadeIn("slow");
			}, 2000);

			$('.track').live('click', function(){
				if (app.Player.inited === false) {
					app.Player.init();
				}
				$('.track.playing').removeClass('playing');
				$(this).addClass('playing');
				app.VK.getFiles($(this));
			});
		},

		getFiles: function(t) {
			$.mobile.loading('show');
			this.artist = t.attr('data-artist');
			this.title = t.attr('data-title'); 
			var q = this.artist+' '+this.title;
			VK.Api.call('audio.search', {q: q, sort: 2, count: 200, offset: 0}, app.VK.onGetFiles);
		},
        
        getTracks: function(name) {
			VK.Api.call('audio.search', {q: name, sort: 2, count: 200, offset: 0}, app.VK.onGetFiles);
        },
			
		onGetFiles: function(data) {
			if (defined(data.error)) {
				console.log("VK error: ", data.error);
				error(data.error);
				app.VK.auth = false;
				VK.Auth.login(null, VK.access.AUDIO);    
				return;
			}

			if (!defined(data.response) || empty(data.response) || data.response[0] === 0) {
				app.VK.skip();
				return;
			}
			var total = data.response[0];
			var sort = [];
			app.VK.sources = [];
			for (key in data.response) {
				var d = data.response[key];
				if (typeof(d.duration) == 'undefined') continue;
				if (typeof(d.title) == 'undefined') continue;
				if (d.duration < 100 || d.duration > 900) continue;
				if (!defined(sort[d.duration])) {
					sort[d.duration] = 1;
					app.VK.sources[d.duration] = [];
				} else {
					sort[d.duration]++;
				} 
				app.VK.sources[d.duration].push({
					url: d.url,
					title: d.title,
					dur: d.duration
				});
			}
			if (empty(app.VK.sources)) {
				app.VK.skip();
				return;
			}
			$.mobile.loading('hide');
			app.VK.duration = arsort(sort, 'SORT_NUMERIC');
			app.VK.current = {src:0, dur:0};
			app.Player.play();
		},

		skip: function(){
			$('#player li.playing').addClass('missing').next().click();
		},
		
		mkTitle: function(q) {
			var r = /\(.*\)/gi;
			var s = q.match(r);
			if (s !== null) {
				return s[0].replace(/\(|\)/g,'');
			} else {
				return q;
			}
		},

	};