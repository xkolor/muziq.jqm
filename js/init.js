var startPages = [""];
if (startPages.indexOf(location.hash) < 0) {
	location.href = "/";
}

var app = new Muziq();
var engine = "LF";
var loaded = false;
$(document).on('ready', function() {
	//$.mobile.changePage('#home');
});

$(document).on('pageinit', '#home', function(e){
	$('.ui-input-search').hide();
	app.init();
    app.LastFm.getCountryTop();
    $('[data-role="panel"]').panel().enhanceWithin();


});

$(document).on('pageinit', '#lists', function(e){
	app.Playlist.RenderAll()
});


function allowDrop(ev){
    ev.preventDefault();
}

function enterDrop(ev) {
    ev.preventDefault();
    $(ev.target).css('opacity',0.6);
}

function exitDrop(ev) {
    ev.preventDefault();
    $(ev.target).css('opacity',1);
}

function drag(ev) {
    ev.dataTransfer.setData("track", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var id = ev.dataTransfer.getData("track");
    $('#'+id).insertBefore($(ev.target).parent());
    $(ev.target).css('opacity',1);
    //$(ev.target).appendChild(document.getElementById(data));
}