Muziq.prototype.Discogs = {
	api: "http://api.discogs.com/",
	found: [],
	artist: null,
	artistUrl: null,
	loaded: false,
	album: null,
		
	
	findArtist: function(q) {
		this.found = [];
		this.artist = q;
		this.artistUrl = null;
		q = encodeURIComponent(q);
		$('.dc-albums').empty().addClass('load4');
		//$.getJSON(this.api+'database/search?type=artist&q='+q+'&callback=?', Discogs.onFindArtist);    
		$.get('dc.php?q='+q, app.Discogs.onFindArtist);
	},

	onFindArtist: function(html) {
		var cards = $(html).find('div.card');
		if (empty(cards)) {
			$('.dc-albums').removeClass('load4');
			return;
		}
		var id = null;
		var next = null;
		var first = $(html).find('div.card').eq(0).data('object-id');
		cards.each(function(k,v){
			var img = $(v).find('.card_image img').attr('src');
			var txt = $(v).find('.card_body h4 a').text();
			if (txt.lc().indexOf(app.Discogs.artist.lc()) > -1) {
				if (img === "http://static.discogs.com/images/default-artist.png" 
				||  img === "http://s.pixogs.com/images/default-artist.png") {
					next = $(v).data('object-id');
				} else {
					id = $(v).data('object-id');
					return false;	
				}
			}
		});
		
		if (!defined(id)) {
			id = defined(next) ? next : first;    
		}

		$.getJSON(app.Discogs.api+'artists/'+id+'/releases?per_page=100&callback=?', app.Discogs.onGetReleases);
		
	},

	onGetReleases: function(e) {
		app.Discogs.found = [];
		var results = '';
		var data = e.data;
		data.releases.reverse();
		$(data.releases).each(function(){
			if (this.role === "Main") {
				if (app.Discogs.found.indexOf(this.title) > -1) {
					return true;
				}
				app.Discogs.found.push(this.title);
				var y = "";
				if (defined(this.year)) {
					y = "<span class='ui-li-count'>"+this.year+"</span>";
				}
				var str =  "<li class='album' data-title='"+ this.title +"' data-year='"+this.year+"'\
									data-url='"+ this.resource_url +"' data-id='"+ this.id+"' >\
									<a>"+ this.title + y +"</a></li>";                
				results += str;
			}            
		});
		
		$('#albums ul.list').append($(results).sort(app.Discogs.sortYear)).listview().listview('refresh');
		$('#albums #albums-for').text(app.LastFm.artist);
		$('#albums .album').click(function(e){
			app.Discogs.album = $(this).data('title');
			var url = $(this).data('url');
			app.Discogs.getTracks(url);
			e.preventDefault();
		});
		app.Discogs.loaded = true;
		if (app.LastFm.showing === false) {
			$.mobile.changePage('#albums');
			$.mobile.loading('hide');
		}
		
	},

	getTracks: function(url){
		$.mobile.loading('show');
		$.getJSON(url+'?callback=?', app.Discogs.onGetTracks);    
	},

	onGetTracks: function(e) {
		var data = e.data;
		var results = '';
		$('#player #tracks-for').text(app.Discogs.album);
		$(data.tracklist).each(function(){
			if (!defined(this.title) || empty(this.title)) {
				return true;
			}
			var d = " ";
			if (defined(this.duration) && !empty(this.duration)) {
				d = "<span class='ui-li-count'>"+ this.duration +"</span>";
			}
            var id = Math.floor(Math.random()*10000);
			results += '<li id="li'+id+'" class="track" data-artist="'+ app.Discogs.artist +'" data-title="'+ this.title +'"'
                     + ' draggable="true" ondragenter="enterDrop(event)" ondragleave="exitDrop(event)" ondragover="allowDrop(event)" ondragstart="drag(event)" ondrop="drop(event)">'
                     + '<a data-artist="'+ app.Discogs.artist +'" data-title="'+this.title+'" class="ui-btn ui-btn-icon-left ui-icon-bars">'
                     + '<div class="ui-add"></div>'
                     + '<span style="line-height:25px">'+ cap(this.title) +'</span>'+ d +"</a></div>";				
		});
		$('#player ul.list').html(results).listview().listview('refresh');
        onSwipe($('#player ul.list .track a'));
		$.mobile.loading('hide');
		$.mobile.changePage("#player");
	},
	
	sortYear: function(a,b) {  
		return $(a).data('year') > $(b).data('year') ? 1 : -1;  
	}  
}